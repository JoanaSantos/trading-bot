# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 17:54:20 2018

@author: Joana Santos
"""

#Part I
#Exercise 1
stock = 'GSK'

stock_value = 6.5

stock_shares = 20

action = 'buy'

print('I would like to', action, stock_shares, 'shares in', stock)

#Exercise 2
borrower = 'Joana'
x=20
person2 = 'Catarina'
y=3
companyZ = 'Tesla'
print(borrower, 'needs to borrow', x, 'euros from', person2, 'in order to trade', y, 'stocks in', companyZ)

#Exercise 3
x=2
y=30
print(x*y)
z=x*y
print(z)

#Exercise 4
celsius=30
conversion_kelvin=celsius+274.15
conversion_fahrenheit=celsius*(9/5)+32
print(conversion_kelvin)
print(conversion_fahrenheit)

#Part II

#Exercise 1
def power (a,b) :
    f = a**b
    return f 
a=2
b=1
c=power(a,b)
print(c)

#Exercise 2
def equality (x,y):
    f = x == y
    return f
t='joana'
e='j'
z=equality (t,e)
print(z)

#Exercise 3
def pythagoras (side_a, side_b):
    hypotenuse = (side_a**2 + side_b**2) ** (1/2)
    return hypotenuse

a=20
b=15
z=pythagoras (a,b)
print (z)

#Exercise 4
def litecoin (b):
    crypto = b * 54.31029
    return crypto
print( litecoin (1) )

def ethereum (b):
    crypto = b * 10.224998
    return crypto
print ( ethereum (3) )

def euros (b):
    crypto = b * 6969.64
    return crypto
print ( euros (20) )

#Exercise 5
#If the function does not have the return statement, the machine will not be 
#able to understand which function to run. Hence the machine will not be guided
# in a proper order to return outputs.
#The return statement terminates the execution of a function and returns control
# to the calling function. Execution resumes in the calling function at the point
# immediately following the call. A return statement can also return a value to 
#the calling function.

#Exerciae 6
#The print() function writes, i.e., "prints", a string in the console. 
#The return statement causes your function to exit and hand back a value to its
# caller. The point of functions in general is to take in inputs and return 
#something. The return statement is used when a function is ready to return a 
#value to its caller.

#Exercise 7
#It happens when you try to input “none type” variables. The output of a print 
#function is a “none type” variable.

  
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 16:56:09 2018

@author: Joana Santos
"""

#Part I
#Exercise 1
bool(1)
#Out[11]: True
bool(1.1)
#Out[12]: True
bool(0)
#Out[13]: False
bool(0.0001)
#Out[14]: True
bool('')
#Out[16]: False
bool ('none')
#Out[17]: True

#Exercise 2
def ticker_symbol (n):
    if n == 'AAPL':
        print ('True')
    else:
        print('False')
ticker_symbol (n='AAPL')

#Exercise 3
def gender (n):
    if n == 'male':
        print ('You like pink')
    else:
        print('You hate pink')
gender (n='male')
#OR…..
def gender (n):
    if n == 'male':
        print ('You like pink')
    elif n == 'female':
        print('You hate pink')
    else: 
        print('not apply')
gender (n='m')

#Exercise 4
def gender (gender):
    if gender == 'male':
        print ('You like pink')
    if gender == 'female':
        print('You hate pink')
    print('not apply')
 gender (gender = 'male')

#Exercise 5
def ages (m, f):
    if m == f:
        print (m+f)
    else:
        print (m-f)
        
ages (50,50)

#Exercise 6
b_list = [1, 3, 6]
b_dict = {'a':1, 'b':2, 'c':3}

def compare (b_list, b_dict):
    if len (b_list) == len (b_dict):
        print ('awesome')
    else:
        print ('that is sad')

compare (b_list, b_dict)

#Exercise 7
ticker_dict = {'APPL':230, 'GOOG':145, 'FB': 80}

for key in ticker_dict:
    print (key)
    
for key in ticker_dict:
    print (ticker_dict[key])

for key in ticker_dict:
    print ('the stock price of', key, 'is', ticker_dict[key])

#Exercise 9
single_list = [1, 2, 3]

def double_list (single_list):
    for num in single_list:
        print (num*2)
    
double_list (single_list)

#Exercise 10
single_dict = {'APPL':100, 'GOOG':90, 'FB':80}

def double_dict (single_dict):
    for key in single_dict:
        print (single_dict[key]*2)
    
double_dict (single_dict)

#Exercise11
list11 = [22, 5, 17, 21]
g = 3

def greater_than (list11, g):
    if len(list11)>g:
        print ('True')
    else:
        print ('')

greater_than (list11, g)

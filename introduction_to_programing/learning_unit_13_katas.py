# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 18:09:37 2018

@author: Joana Santos
"""

#Kata1
 
'''
Scope - The place where a variable is visible and can be used
Local Scope - The variable is not available on the whole document, it can only 
be used in a specific limited space, such as inside a function
Global Scope - The variable can be used inside the whole document

'''

#Kata 2

a = 2

def no_arguments ():
    return a*2

b = no_arguments ()

print (a,b)


# Kata 3

a_list = [1,2]

def two_arguments (a_list, a):
    if len (a_list) > a:
        return True

print (two_arguments (a_list, a))


# Kata 4

value = 5

def print_this (value):
    print (value)

print_this (value)


# Kata 5

def return_this (value):
    return value

print (return_this (value))


# Kata 6

'''

Python function always return something, if it is not defined by the user, 
returns None

Print displays its arguments on the screen 
Return is used to return a value of a function
'''


# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 18:49:52 2018

@author: Joana Santos
"""
#Functional Programming is based on the premise of using functions as data/values
#we can use functions as arguments of other functions
def anarchy ():
    print ('arnarchy')

some_func = anarchy
some_func()

#How many functions are being called? 
# One time only because we only use () one time
# You are assign/store the function to another variable making this new variable into a function

rainbows_and_puppies = anarchy

anarchy ()
rainbows_and_puppies ()

#We are calling the same function

def func_caller (func):
    func()

def law_and_order ():
    print ('law and order')

func_caller (law_and_order)

#Exercise 1
print ('Exercise 1')
def main_func (arg_func):
    arg_func()

def some_func ():
    print ('nothing to say')

print (main_func (some_func))

#Exercise 2
print ('Exercise 2')
def two_func (func1, func2):
    func1()
    func2()

print (two_func(anarchy,some_func))

#Exercise 3
print ('Exercise 3')
def func3 (integer, argument3):
    if integer > 10:
        argument3()
    
print (func3(20, anarchy))
print (func3(3, anarchy))

#Exercise 4
print ('Exercise 4')
def func4 (arg1, arg2):
    if arg1:
        arg2()

print (func4(rainbows_and_puppies, anarchy))

#Exercise 5
print ('Exercise 5')
def func5 (sub_func):
    return sub_func()

def sub_func ():
    print ('AH')

print (func5(sub_func))

#Exercise 6
print ('Exercise 6')
def func6 (sub1, sub2):
    if sub1 ():
        return sub2 ()

def true_function():
    return True
def false_function():
    return False
def sub2():
    return 43

print(func6(true_function, sub2))
print(func6(false_function, sub2))    




# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 17:54:57 2018

@author: Joana Santos
"""

# EXERCISE ONE
# Write a function called stock_fun that takes a single argument. This function should
# return True if the value of the argument is 'GOOGLE' or 'TSLA'.
# Return False otherwise

def stock_fun(ticker):
    if ticker == 'GOOGLE' or ticker == 'TSLA':
        return True
    else:
        return False

#EXERCISE TWO
# Write a function called comparing_ages that takes two arguments: 
# The first argument should be your age 
# The second should be the age of your mother. 

# The function should Return True if the difference between your age and your 
# mother's is bigger than 20. Return False otherwise.

def comparing_ages(my_age, mother_age):
    if my_age - mother_age > 20:
        return True
    else:
        return False

# EXERCISE THREE
# Write a function called list_slice that takes a list as an argument and returns the second 
# element of that list. Define a list [1,2,3] as the default value for your argument
        
def list_slice (list3 = [1,2,3]):
    return list3[1]

print (list_slice ())

# EXERCISE FOUR
# Write a function called double_dict that takes a single dictionary as an argument that maps a 
# string to a number. The function should return a new dictionary that multiplies
# each value of the input dictionary by 2

def double_dict (dict4):
    for key in dict4:
        dict4[key] = dict4[key] * 2
        return dict4

dict4 = {'AAPL':1, 'GOOG':2}

print (double_dict (dict4))


# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 17:39:26 2018

@author: Joana Santos
"""

#Kata1

dictionary = {'APPL':100, 'GOOG':90, 'FB':80}

def multiply (dictionary):
    for key in dictionary:
        dictionary[key] = dictionary[key]*2
    return dictionary

print(multiply(dictionary))

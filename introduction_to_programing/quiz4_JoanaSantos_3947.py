# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 13:46:03 2018

@author: Joana Santos
"""

# EXERCISE ONE
# Write a function called stock_fun that takes a single argument. This function should
# return True if the value of the argument is 'GOOGLE' or 'TSLA'.
# Return False otherwise

def stock_fun(ticker):
    if ticker == 'GOOGLE' or ticker == 'TSLA':
        return True
    else:
        return False
        

print (stock_fun (ticker = 'AAPL')

# EXERCISE TWO
# Write a function called comparing_ages that takes two arguments: 
# The first argument should be your age 
# The second should be the age of your mother. 

# The function should Return True if the difference between your age and your 
# mother's is bigger than 20. Return False otherwise.

def comparing_ages(my_age, mother_age):
    if my_age-mother_age > 20:
        return True
    else:
        return False
        
print (comparing_ages (my_age = 22, mother_age = 48))

# EXERCISE THREE
# Write a function called list_slice that takes a list as an argument and returns the second 
# element of that list. Define a list [1,2,3] as the default value for your argument 

def list_slice (list3 = [1,2,3]):
    return list3[1]

print (list_slice ())

# EXERCISE FOUR
# Write a function called power_list that take a list as an argument. This function, should call 
# the list_slice() with the same argument and return the same value divided 
# by 2. 
    
# Hint: The argument you pass on power_list is the same you pass on list_slice()

def power_list (list4):
    return list_slice (list4)/2
            

print (power_list ([1,3,4,5]))


# EXERCISE FIVE
# Write a function called list_and_number that takes two arguments: 
# The first is a single number
# The second is list 

# If this number is greater than 10, append that number to the list and return 
# the new list. Otherwise return the number. 

def list_and_number (number, list5):
    if number > 10:
        list5.append (number)
        return list5
    else:
        return number

number = 20
list5 = [1, 3, 5]

print(list_and_number (number, list5))

# EXERCISE SIX
# Write a function called compare_list that takes two arguments as its input.
    # The first argument should be a list
    # The second argument should be a number
# The function should return True if the lenght of the list is greater 
# than the number that was passed in as an argument. Return false otherwise
    
def compare_list (list6, n):
    if len (list6)>n:
        return True
    else:
        return False

list6 = [2, 4, 6]
n = 2

compare_list (list6, n)

# EXERCISE SEVEN
# Write a function called double_dict that takes a single dictionary as an argument that maps a 
# string to a number. The function should return a new dictionary that multiplies
# each value of the input dictionary by 2

def double_dict (dict7):
    for key in dict7:
        return dict7[key]*2

dict7 = {'AAPL':1, 'GOOG':2}

print (double_dict (dict7))

# EXERCISE EIGHT
# Consider the follwing dictionary
stock_dict = {'AAPL':100, 'GOOG':800}

# Write a function called return_value that takes two arguments: 
    # the first one is a variable with the ticker of a stock, by default should be 'AAPL'
    # the second is a dictionary, by default should be stock_dict
    
# The variable with the ticker of the stock it's a key to the dictionary. The function
# should return the value of this key
    
def return_value (ticker_symbol= 'AAPL', dic = stock_dict)
    print (dic[ticker_symbol])
    
print (return_value ())




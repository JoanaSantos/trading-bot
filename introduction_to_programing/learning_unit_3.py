# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 16:42:08 2018

@author: Joana Santos
"""

#Part I
#Exercise1
#Scope – it is about the visibility of variables, where they are, if it is possible to use it
#Local scope – inside a function
#Global scope – outside the function body

#Exercise2

#Exercise3
 # File "C:/Users/Joana Santos/Desktop/untitled0.py", line 24, in <module>
    print ('hello', first_var)
NameError: name 'first_var' is not defined
#It happens because first_var is inside a function scope. So, it cannot be used on the global scope??

#Exercise4
global_var = 10
def normal_function ():
    x = global_var*2
    return x
new_var = normal_function ()
print (new_var)

#Exercise5
#The variables define outside work in the levels inside but not in the other way around.

#Exercise6
power = 10
print (power)

def generate_power (number):
    power = 2
    def nth_power ():
        return number**power
    powered = nth_power ()
    return powered
print (power)  
a = generate_power (2)
print (a)

#Exercise 7
_what = 'coins'
_for = 'chocolates'
def transaction (_what,_for):
    _what = 'oranges'
    _for = 'bananas'
    print (_what, _for)
transaction (_what, _for)
print (_what, _for)


#Exercise 9/10
fn = 'Joana'
ln = 'Santos'

def name (first_name = 'Sam', last_name = 'Hopkins'):
    print (first_name, last_name)

name ()
name (last_name = 'Jose', first_name = 'Maria')
name (first_name = fn, last_name = ln)

#Exercise 11
def statement (message = 'Good morning', name = ''):
    print (message, name)

statement ()
statement (message ='You know nothing', name = 'John Snow')


#Part II
#Exercise 0
my_tuple = ('a', 'b', 'c')
print (my_tuple [2])
print (my_tuple [0])
print (my_tuple [1])

#Exercise 1
#Não dá! Nas tuples não dá para mudar nada

#Exercise 2
APPL_tuple = (150, 210, 230, 400, 690)
print (type(APPL_tuple), APPL_tuple)

#Exercise 3
#Nao da. A tuple é imutavel

#Exercise 4
list_APPL = [1 ,2, 3, 4, 5]
list_APPL.append(6)
print (list_APPL)

#Exercise 5
#A tuple is immutable. Lists, you can manipulate them.

#Exercise 6
(1,2,3) == [1,2,3]
#Out[32]: False
(1,2,3) == (2,1,3)
#Out[33]: False
[1,2,3] == [1,2,3]
#Out[34]: True
[1,2,3] == [1,3,2]
#Out[35]: False

#Exercise 7
tuple1 = (1,2,3)
list1 = list(tuple1)
print(list1)
